import os

from solid import *
from solid.utils import *
from solidpylib.hardware.screw_flat_head import ScrewFlatHeadM3
from solidpylib.hardware.nut import NutM3


class Configuration:
    pole_diameter = 5.2  # 5.5 is the standard screw diameter
    pole_socket_overhang = 3
    pole_height = 8
    pole_distance = 71.5
    fan_width = 80
    fan_height = 25
    fan_bottom_distance = 8
    fan_top_distance = 0.4
    wall_thickness = 2.2
    outer_corner_radius = 5
    bottom_thickness = 2
    top_thickness = 2
    fan_side_clearance = 0.2
    shell_clearance = 0.2
    lower_shell_overlap = 10
    shell_gap = 0.4
    fan_effective_radius = 34
    grill_inner_circle_radius = 8
    grill_radial_width = 2
    grill_concentric_width = 1
    grill_concentric_positions = [6 + i * 10 for i in [1, 2]]
    grill_number_radial_stripes = 5
    grill_radial_stripe_start_angle = 0
    original_radius = 30
    original_screw_distance = 50
    original_screw_hole_diameter = 3.2
    original_screw_hole_corners = [(-1, 1), (1, -1)]
    xy_offset = -5, -10
    connectors_side_clearance = 0.1
    connectors_top_clearance = 0.2
    connectors_width = 8
    connectors_height = 10
    connectors_nut_depth = 3
    connectors_part_lengths = [6, 9, 6]  # middle, nut part, screw head

    @property
    def connectors_full_length(self):
        return sum(self.connectors_part_lengths)

    @property
    def inner_corner_radius(self):
        return self.outer_corner_radius - self.wall_thickness - self.shell_clearance

    @property
    def lower_height(self):
        return self.bottom_thickness + self.fan_bottom_distance + self.lower_shell_overlap

    @property
    def upper_height(self):
        return self.top_thickness + self.fan_height - self.shell_gap

    @property
    def xy_3d_offset(self):
        return self.xy_offset[0], self.xy_offset[1], 0

    @property
    def width(self):
        return self.fan_width + 2 * (self.fan_side_clearance + self.shell_clearance + 2*self.wall_thickness)

    @property
    def inner_width(self):
        return self.fan_width + 2 * (self.fan_side_clearance + self.wall_thickness)

    @property
    def center_xy(self):
        hw = self.width / 2
        return hw, hw, 0

    @property
    def pole_side_distance(self):
        return (self.width - self.pole_distance) / 2

    @property
    def inner_xy_offset(self):
        o = self.wall_thickness + self.shell_clearance
        return o, o, 0


c = Configuration()


class Frame:

    def __init__(self, w, h, rounding_r, wall_thickness, bottom_thickness):
        self.w = w
        self.h = h
        self.rounding_r = rounding_r
        self.wall_thickness = wall_thickness
        self.bottom_thickness = bottom_thickness

    @property
    def footprint(self):
        return translate((self.rounding_r, self.rounding_r, 0))(
            minkowski()(
                square((self.w - 2*self.rounding_r, self.w - 2*self.rounding_r)),
                circle(r=self.rounding_r, segments=100)
            )
        )

    @property
    def cutaway_footprint(self):
        return offset(r=-self.wall_thickness)(self.footprint)

    def __call__(self):
        return up(0)(
            linear_extrude(self.h)(self.footprint)
            -
            up(self.bottom_thickness)(linear_extrude(self.h)(self.cutaway_footprint))
        )


class Poles:
    def __init__(self, base_height):
        self.base_overhang = base_height

    @property
    def single_pole(self):
        h = self.base_overhang + c.pole_height - c.pole_diameter/2
        w = 40
        return up(0)(
            linear_extrude(self.base_overhang)(
                translate((c.pole_socket_overhang, c.pole_socket_overhang, 0))(
                    translate((-w, -w, 0))(
                        minkowski()(
                            square((w, w)),
                            circle(d=c.pole_diameter, segments=50)
                        )
                    )
                )
            )
            +
            cylinder(d=c.pole_diameter, h=h, segments=50)
            +
            up(h)(sphere(d=c.pole_diameter, segments=50))
        )

    def __call__(self):
        sd = c.pole_side_distance
        opposite_sd = c.width - sd
        return sum([
            translate((sd, sd, 0))(
                rotate((0, 0, 0))(self.single_pole)
            ),
            translate((sd, opposite_sd, 0))(
                rotate((0, 0, 270))(self.single_pole)
            ),
            translate((opposite_sd, opposite_sd, 0))(
                rotate((0, 0, 180))(self.single_pole)
            ),
            translate((opposite_sd, sd, 0))(
                rotate((0, 0, 90))(self.single_pole)
            )
        ])


class FanGrill:

    @property
    def radial_angles(self):
        _offset = 360 / c.grill_number_radial_stripes
        return [c.grill_radial_stripe_start_angle + i * _offset for i in range(c.grill_number_radial_stripes)]

    @property
    def radials(self):
        return sum([
            rotate(a)(
                left(c.grill_radial_width/2)(square((c.grill_radial_width, c.fan_effective_radius)))
            ) for a in self.radial_angles
        ])

    @property
    def concentrics(self):
        h = c.grill_concentric_width / 2
        return sum([
            circle(r=_r + h, segments=100) - circle(r=_r - h, segments=100) for _r in c.grill_concentric_positions
        ])

    @property
    def footprint(self):
        return up(0)(
            circle(r=c.grill_inner_circle_radius, segments=100)
            +
            self.radials
            +
            self.concentrics
        )

    def __call__(self):
        return linear_extrude(c.top_thickness)(
            self.footprint
        )


class OriginalCutout:

    @property
    def footprint(self):
        o = c.original_screw_distance / 2
        return up(0)(
            circle(c.original_radius, segments=100)
            +
            sum([
                translate((_x*o, _y*o, 1))(
                    circle(c.original_screw_hole_diameter, segments=50)
                ) for _x, _y in c.original_screw_hole_corners
            ])
        )

    def __call__(self):
        return down(1)(
            linear_extrude(c.bottom_thickness + 2)(
                self.footprint
            )
        )


class LowerConnectors:
    def __call__(self):
        return self.first_connector + self.second_connector

    @property
    def second_connector(self):
        return forward(c.width)(
            mirror((0, 1, 0))(
                self.first_connector
            )
        )

    @property
    def first_connector(self):
        s = ScrewFlatHeadM3(clearance=0.2, length=50, head_elongation=10)
        n = NutM3(clearance=0.2, height=10)
        w_middle = c.connectors_part_lengths[1] + 2 * c.connectors_side_clearance
        return up(0)(
            back(c.connectors_width)(
                right(c.width/2)(
                    right(-c.connectors_full_length/2)(
                        cube((c.connectors_full_length, c.connectors_width, c.connectors_height))
                    )
                    -
                    right(c.connectors_full_length/2 - 0.2)(
                        forward(c.connectors_width/2)(
                            up(c.connectors_height/2)(
                                rotate((0, 90, 0))(
                                    s()
                                )
                            )
                        )
                    )
                )
            )
            -
            right(c.connectors_nut_depth)(
                right(c.width/2 - c.connectors_full_length/2 - 10)(
                    back(c.connectors_width/2)(
                        up(c.connectors_height/2)(
                            rotate((0, 90, 0))(
                                color("red")(n())
                            )
                        )
                    )
                )
            )
            -
            right(c.connectors_part_lengths[0] + c.connectors_side_clearance)(
                translate(((c.width - c.connectors_full_length)/2, -c.connectors_width - 1, -1))(
                    color("purple")(cube((w_middle, c.connectors_width + 1, c.connectors_height + 2)))
                )
            )
        )


class LowerShell:
    def __call__(self):
        frame = Frame(c.width, c.lower_height, c.outer_corner_radius, c.wall_thickness, c.bottom_thickness)
        poles = intersection()(
            Poles(c.bottom_thickness + c.fan_bottom_distance)(),
            linear_extrude(100)(frame.footprint)
        )
        cutout = OriginalCutout()
        connectors = LowerConnectors()
        return up(0)(
            frame()
            +
            poles
            +
            connectors()
            -
            translate(c.xy_3d_offset)(translate(c.center_xy)(cutout()))
        )


class UpperShell:
    def __call__(self):
        frame = Frame(c.inner_width, c.upper_height, c.inner_corner_radius, c.wall_thickness, c.top_thickness)
        poles = intersection()(
            Poles(c.top_thickness + c.fan_top_distance)(),
            translate(c.inner_xy_offset)(linear_extrude(100)(frame.footprint))
        )
        grill = FanGrill()

        return up(0)(
            translate(c.inner_xy_offset)(frame())
            +
            poles
            -
            translate(c.center_xy)(
                down(1)(
                    cylinder(r=c.fan_effective_radius, h=c.top_thickness + 2, segments=100)
                )
            )
            +
            translate(c.center_xy)(
                grill()
            )
        )


class Renderer:

    def __init__(self, output_prefix):
        self.output_prefix = output_prefix

    def __call__(self, scad_object, base, name):
        path = os.path.join(self.output_prefix, base)
        if not os.path.isdir(path):
            os.makedirs(path)
        scad_render_to_file(scad_object, os.path.join(path, name + ".scad"))


r = Renderer("scad")

f = Frame(100, 20, 5, 2, 2)
p = Poles(5)

lower = LowerShell()
upper = UpperShell()

r(
    upper(),
    "print",
    "upper"
)

r(
    lower(),
    "print",
    "lower"
)

r(
    f(),
    "test",
    "frame"
)

r(
    p(),
    "test",
    "pole"
)
